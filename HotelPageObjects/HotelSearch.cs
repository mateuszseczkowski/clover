﻿using CommonBase;
using OpenQA.Selenium;

namespace HotelPageObjects
{
    public class HotelSearch : RydooPageWithMap //PageFromBaseProject will include also the driver, it will be in 'Base' project.
    {
        private IWebElement _locationInput => Driver.FindElement(By.CssSelector(".location");
        private IWebElement _checkInDateInput => Driver.FindElement(By.CssSelector(".checkin");
        private IWebElement _checkOutDateInput => Driver.FindElement(By.CssSelector(".checkout");
        private IWebElement _searchButton => Driver.FindElement(By.CssSelector(".checkout");

        public void SetLocation(string location)
        {
            _locationInput.SendKeys(location);
        }
        public void SetCheckInDate(string checkInDate)
        {
            _checkInDateInput.SendKeys(checkInDate);
        }
        public void SetCheckOutDate(string checkOutDate)
        {
            _checkOutDateInput.SendKeys(checkOutDate);
        }

        public HotelList Search()
        {
            _searchButton.Click();
            return new HotelList();
        }
    }
}
