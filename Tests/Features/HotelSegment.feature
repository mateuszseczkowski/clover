﻿Feature: Hotel segment

@hotelonly
Scenario: Create Hotel booking as a selfbooker
	Given I am logged in as a selfbooker
	When I go to the Travel page
	And I click the Hotel icon
	And I search for hotels for 'Paris, France' with check-in in '3' days and length '2'
	And I select the first available room and book it
	Then I have a new reservation made only with the Hotel segment and it is in a 'Confirmed' status