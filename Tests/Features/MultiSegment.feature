﻿Feature: Multi segment trip

@multisegment
Scenario: Create a trip with air and hotel segments
	Given I am logged in as a selfbooker
	When I create a new trip with a hotel segment
	And I add to the trip a flight
	Then I have a new multi segment trip combining airs and hotels