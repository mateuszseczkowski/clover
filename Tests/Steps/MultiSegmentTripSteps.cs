﻿using System;
using TechTalk.SpecFlow;

namespace Tests.Steps
{
    [Binding]
    public class MultiSegmentTripSteps
    {
        [When(@"I create a new trip with a hotel segment")]
        public void WhenICreateANewTripWithAHotelSegment()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I add to the trip a flight")]
        public void WhenIAddToTheTripAFlight()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I have a new multi segment trip combining airs and hotels")]
        public void ThenIHaveANewMultiSegmentTripCombiningAirsAndHotels()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
