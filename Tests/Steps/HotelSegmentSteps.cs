﻿using BusinessLogic;
using HotelPageObjects;
using TechTalk.SpecFlow;

namespace Tests.Steps
{
    [Binding]
    public class HotelSegmentSteps
    {
        private HotelBusinessLogic h_bl;
        private HotelSearch hotelSearch;
        private HotelList hotelList;

        public HotelSegmentSteps()
        {
            hotelSearch = new HotelSearch();
            hotelList = new HotelList();
        }

        [When(@"I go to the Travel page")]
        public void WhenIGoToTheTravelPage()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I click the Hotel icon")]
        public void WhenIClickTheHotelIcon()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I search for hotels for '(.*)' with check-in in '(.*)' days and length '(.*)'")]
        public void WhenISearchForHotelsForWithCheck_InInDaysAndLength(string p0, int p1, int p2)
        {
            //...
            hotelSearch.Search();
        }
        
        [When(@"I select the first available room and book it")]
        public void WhenISelectTheFirstAvailableRoomAndBookIt()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I have a new reservation made only with the Hotel segment and it is in a '(.*)' status")]
        public void ThenIHaveANewReservationMadeOnlyWithTheHotelSegmentAndItIsInAStatus(string p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}
