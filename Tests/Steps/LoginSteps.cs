﻿using TechTalk.SpecFlow;

namespace Clove.Steps
{
    [Binding]
    public class LoginSteps
    {
        private readonly ScenarioContext _scenarioContext;

        [Given(@"I am on Rydoo login page")]
        public void GivenIAmOnRydooLoginPage()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"I am logged in as a selfbooker")]
        public void GivenIAmLoggedInAsASelfbooker()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I insert login: '(.*)' and click")]
        public void WhenIInsertLoginAndClick(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I insert password: '(.*)'")]
        public void WhenPassword(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I click Login button")]
        public void WhenIClickLoginButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I am logged in")]
        public void ThenIAmLoggedIn()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
