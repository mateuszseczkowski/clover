﻿using System;
using TechTalk.SpecFlow;

namespace Tests.Steps
{
    [Binding]
    public class AirSegmentFeatureSteps
    {
        [Given(@"a")]
        public void GivenA()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"b")]
        public void WhenB()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"c")]
        public void ThenC()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
